extern crate tui;
extern crate termion;

use std::io;

use termion::raw::IntoRawMode;
use tui::Terminal;
use tui::backend::TermionBackend;
use tui::widgets::{Widget, Block, Text, Paragraph, Borders};
use tui::layout::{Layout, Constraint, Direction};


fn main() {
    let stdout = io::stdout().into_raw_mode().unwrap();
    let backend = TermionBackend::new(stdout);
    let mut term = Terminal::new(backend).unwrap();
    term.clear().unwrap();
    let size = term.size().unwrap();

    // The line will be drawn over the border and panic once it tries to write outside of the screen.
    for len in 1..size.width {
        let label = format!("Chars = {}, width = {}", len, size.width);
        let line: String = std::iter::repeat('a').take(len.into()).collect();
        let contents = [Text::raw(&line)];
        term.draw(|mut f| {
            let area = Layout::default()
                .direction(Direction::Horizontal)
                .margin(2)
                .constraints([Constraint::Percentage(100)].as_ref())
                .split(size);
            Paragraph::new(contents.iter())
                .block(Block::default().title(&label).borders(Borders::ALL))
                .wrap(false)
                .render(&mut f, area[0]);
        }).unwrap();
    }
}
